const db = require("../models");
const Branch = db.Branch;
const Account = db.Account;
const Op = db.Op;
exports.create = (req, res) => {
  const logedUser = req.decoded.result;
  const body = req.body;
  if (!req.body.name) {
    res.status(400).send({
      message: "name can not be empty!",
    });
    return;
  }

  const branch = {
    name: body.name,
    accountId: logedUser.accountId,
  };

  Branch.create(branch)
    .then((Data) => {
      res.send(Data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Branch.",
      });
    });
};
exports.getAll = (req, res) => {
  const logedUser = req.decoded.result;
  Branch.findAll({
    where: { accountId: logedUser.accountId },
    include: [Account],
  })
    .then((Data) => {
      res.status(200).send({
        Data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while getting the Branches.",
      });
    });
};
exports.getBranch = (req, res) => {
  const id = req.params.id;
  const logedUser = req.decoded.result;
  Branch.findAll({
    where: { accountId: logedUser.accountId, id: id },
    include: [Account],
  })
    .then((Data) => {
      res.status(200).send({
        Data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while geting the Branch.",
      });
    });
};
exports.update = (req, res) => {
  const logedUser = req.decoded.result;
  const id = req.params.id;
  const body = req.body;
  const branch = {
    name: body.name,
    updatedAt: Date.now(),
    updatedBy: logedUser.username,
  };
  return Branch.update(branch, {
    where: {
      id: id,
    },
  })
    .then((Data) => {
      if (Data[0] == 0) {
        res.status(404).send({
          message: "data not found",
        });
      } else {
        Branch.findByPk(id, { include: Account })
          .then((Data) => {
            res.send(Data);
          })
          .catch((err) => {
            res.status(500).send({
              message:
                err.message || "Some error occurred while getting the Branch.",
            });
          });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while updating the Branch.",
      });
    });
};
