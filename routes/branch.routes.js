module.exports = app => {
    const branch = require("../controllers/branch.controller");
    const { checkToken } = require("../auth/token_validation");

    var router = require("express").Router();

    router.post("",checkToken, branch.create)
    router.get("",checkToken, branch.getAll)
    router.get("/:id",checkToken, branch.getBranch);
    router.patch("/:id",checkToken, branch.update);

    app.use("/api/v1/branches", router);

}