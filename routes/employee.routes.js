module.exports = app => {
    const empoloyee = require("../controllers/employee.controller");
    const { checkToken } = require("../auth/token_validation");

    var router = require("express").Router();

    router.post("",checkToken, empoloyee.create)
    router.get("",checkToken, empoloyee.getEmployees)
    router.get("/:id",checkToken, empoloyee.getEmployee);
    router.patch("/:id",checkToken, empoloyee.update);

    app.use("/api/v1/employees", router);

}