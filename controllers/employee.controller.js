const db = require("../models");
const fs = require('fs');
const Employee = db.Employee;
const Account = db.Account;
const Op = db.Op;

exports.create = (req, res) => {
  const logedUser = req.decoded.result;
  const body = req.body;
  let name ;
  if (!req.body.nic) {
    res.status(400).send({
      message: "NIC can not be empty!",
    });
    return;
  }
  if(req.body.picture != undefined || req.body.picture != null ) {
    try{
      name = Date.now()+'.png'
      const path = './uploads/'+name;
      const imgdata = req.body.picture;
      const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');
      fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
    }catch(e){
      res.status(500).send(e);
    }
  }else{
    name = null;
  }
  const employee = {
    firstname: body.firstname,
    lastname: body.lastname,
    address: body.address,
    address2: body.address2,
    gender: body.gender,
    email: body.email,
    nic: body.nic,
    phone: body.phone,
    phone2: body.phone2,
    emergency_contact: body.emergency_contact,
    joined_date: body.joined_date,
    picture:name,
    birth_date: body.birth_date,
    position: body.position,
    accountId: logedUser.accountId,
  };

  Employee.create(employee)
    .then((Data) => {
      res.send(Data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Creating the employee",
      });
    });
};
exports.getEmployees = (req, res) => {
  const logedUser = req.decoded.result;
  Employee.findAll({
    where: { accountId: logedUser.accountId },
    include: [Account],
  })
    .then((Data) => {
      res.status(200).send({
        Data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while getting the employees",
      });
    });
};
exports.getEmployee = (req, res) => {
  const id = req.params.id;
  const logedUser = req.decoded.result;
  Employee.findAll({
    where: { accountId: logedUser.accountId, id: id },
    include: [Account],
  })
    .then((Data) => {
      res.status(200).send({
        Data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while getting the Employee.",
      });
    });
};

exports.update = (req, res) => {
  const id = req.params.id;
  const body = req.body;
  let path;
  try{
    path = './uploads/'+Date.now()+'.png'
    const imgdata = req.body.picture;
    const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');
    fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
  }catch(e){
    res.status(500).send(e);
  }
  const employee = {
    firstname: body.firstname,
    lastname: body.lastname,
    address: body.address,
    address2: body.address2,
    picture:path,
    gender: body.gender,
    email: body.email,
    nic: body.nic,
    phone: body.phone,
    phone2: body.phone2,
    emergency_contact: body.emergency_contact,
    joined_date: body.joined_date,
    birth_date: body.birth_date,
    position: body.position,
  };
  return Employee.update(employee, {
    where: {
      id: id,
    },
  })
    .then((Data) => {
      if (Data[0] == 0) {
        res.status(404).send({
          message: "data not found",
        });
      } else {
        Employee.findByPk(id, { include: Account })
          .then((data) => {
            res.send(data);
          })
          .catch((err) => {
            res.status(500).send({
              message:
                err.message ||
                "Some error occurred while getting the employee.",
            });
          });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while updating the employee.",
      });
    });
};
// exports.upload = (req, res) => {
//   try {
//     if (!req.body.image) {
//       res.send({
//         status: false,
//         message: "No file uploaded",
//       });
//     } else {
//       //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
//       let avatar = req.body.image;

//       //Use the mv() method to place the file in the upload directory (i.e. "uploads")
//       const path = './uploads/'+Date.now()+'.png'
//       const imgdata = req.body.image;
//       const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, '');
//       fs.writeFileSync(path, base64Data,  {encoding: 'base64'});
//       return res.send(path);

//     }
//   } catch (err) {
//     res.status(500).send(err);
//   }
// };
