require("dotenv").config();
const express = require("express");
const cors = require("cors");
const db = require("./models");

const app = express();
app.use(cors());


app.use(express.json());

require("./routes/user.routes")(app);
require("./routes/employee.routes")(app);
require("./routes/branch.routes")(app);
require("./routes/department.routes")(app);

app.use(express.static('uploads'));

const port = process.env.PORT ;

db.sequelize.sync({alter:true});

app.listen(port, () => {
  console.log("server up and running on PORT :", port);
});


