const db = require("../models");
const { hashSync, genSaltSync, compareSync } = require("bcrypt");
const nodemailer = require("nodemailer");
const Users = db.Users;
const Role  = db.Role;
const Account = db.Account;
const { sign } = require("jsonwebtoken");

const Op = db.Op;

exports.create = (req, res) => {
  const body = req.body;
  const salt = genSaltSync(4);
  body.password = hashSync(body.password, salt);
  if (!req.body.email) {
    res.status(400).send({
      message: "Email can not be empty!",
    });
    return;
  }

  const user = {
    firstname: body.firstname,
    lastname: body.lastname,
    gender: body.gender,
    email: body.email,
    number: body.number,
    password: body.password,
    username: body.username,
    roleId: body.roleId,
    accountId:body.accountId
  };

  Users.create(user)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the User.",
      });
    });
};

exports.login = (req, res) => {
  const body = req.body;
  if (!body.username) {
    res.status(400).send({
      message: "username can not be empty!",
    });
    return;
  }
  Users.findOne({ where: { username: body.username },include:[Role,Account]})
    .then((results) => {
      const result = compareSync(body.password, results.dataValues.password);
      if (result) {
        results.password = undefined;
        const jsontoken = sign({ result: results }, process.env.JWT_KEY, {
          expiresIn: "18h",
        });
        return res.json({
          message: "login successfully",
          token: jsontoken,
        });
      } else {
        return res.status(401).json({
          message: "Invalid email or password",
        });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};
var transpoter = nodemailer.createTransport({
  service: "gmail",
  secure: false,
  auth: {
    user: process.env.EMAIL,
    pass: process.env.PASSWORD,
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false,
  },
});
exports.foget = (req, res) => {
  const body = req.body;
  if (!body.email) {
    res.status(400).send({
      message: "username can not be empty!",
    });
    return;
  }
  Users.findOne({ where: { email: body.email } })
    .then((results) => {
      var mailOptions = {
        from: process.env.EMAIL,
        to: "madurangadinal@gmail.com",
        subject: "Password Cafe Management system",
        // html:<p><b>Your Login Details For Cafe Manegement System</b><br></br><b><b>Email:</b>'+results[0].email+'<br></br><b>Password:</b>'+results[0].password+'<br></br><a href="localhost:8080/user/login">Click here to login</a></b></p>
      };
      transpoter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log("Email sent" + info.response);
        }
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
