module.exports = (sequelize, Sequelize) => {
  const Employee = sequelize.define("employee", {
    firstname: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: "firstname cannot be empty",
        },
      },
    },
    lastname: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: "lastname cannot be empty",
        },
      },
    },
    address: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: "address cannot be empty",
        },
      },
    },
    address2: {
      type: Sequelize.STRING,
    },
    gender: {
      type: Sequelize.STRING(10),
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: "gender cannot be empty",
        },
      },
    },
    email: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: "email cannot be empty",
        },
      },
    },
    nic: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: "NIC cannot be empty",
        },
      },
    },
    phone: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: "Phone cannot be empty",
        },
      },
    },
    phone2: {
      type: Sequelize.STRING,
    },
    emergency_contact: {
      type: Sequelize.STRING,
    },
    picture: {
      type: Sequelize.STRING,
    },
    joined_date: {
      type: Sequelize.DATE,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: "joined date cannot be empty",
        },
      },
    },
    birth_date: {
      type: Sequelize.DATE,
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg: "birth date cannot be empty",
        },
      },
    },
    position: {
      type: Sequelize.STRING,
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
    },
    enable:{
      type: Sequelize.BOOLEAN,
    }
  });

  return Employee;
};
