module.exports = app => {
    const department = require("../controllers/department.controller");
    const { checkToken } = require("../auth/token_validation");

    var router = require("express").Router();

    router.post("",checkToken, department.create)
    router.get("",checkToken, department.getAll)
    router.get("/:id",checkToken, department.getBranch);
    router.patch("/:id",checkToken, department.update);

    app.use("/api/v1/departments", router);

}