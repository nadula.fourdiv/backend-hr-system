const dbConfig = require("../config/db.config.js");

const {Sequelize, DataTypes, Op} = require("sequelize");
// @ts-ignore
Date.prototype.toJSON = function(){ return this.toLocaleString(); }


const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  logging: false,
  host: dbConfig.HOST,
  // @ts-ignore
  port: dbConfig.PORT,
    // @ts-ignore
  dialect: dbConfig.dialect,
    // @ts-ignore
  operatorsAliases: 0,
  timezone: '+05:30',
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.Op = Op;


db.Users = require("./user.model")(sequelize, Sequelize);
db.Employee = require("./empolyee.model")(sequelize, Sequelize);
db.Role = require("./role.model")(sequelize, Sequelize);
db.Account = require("./account.model")(sequelize, Sequelize);
db.Branch = require("./branch.model")(sequelize, Sequelize);
db.Department = require("./department.model")(sequelize, Sequelize);

db.Role.hasMany(db.Users);
db.Users.belongsTo(db.Role, {
  foreignKey: "roleId",
});
db.Account.hasMany(db.Users);
db.Users.belongsTo(db.Account, {
  foreignKey: "accountId",
});

db.Account.hasMany(db.Employee);
db.Employee.belongsTo(db.Account, {
  foreignKey: "accountId",
});

db.Account.hasMany(db.Branch);
db.Branch.belongsTo(db.Account, {
  foreignKey: "accountId",
});

db.Branch.hasMany(db.Department);
db.Department.belongsTo(db.Branch, {
  foreignKey: "branchId",
});
db.Account.hasMany(db.Department);
db.Department.belongsTo(db.Account, {
  foreignKey: "accountId",
});

module.exports = db;